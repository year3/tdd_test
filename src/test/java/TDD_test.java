

import com.ntc.lab031.Add;
import com.ntc.lab031.RPS;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author L4ZY
 */
public class TDD_test {

    public TDD_test() {
    }

    @Test
    public void testAdd_1_1is2() {
        assertEquals(2, Add.add(1, 1));
    }

    @Test
    public void testAdd_1_2is3() {
        assertEquals(3, Add.add(1, 2));
    }
        @Test
    public void testRPS_p1_p_p2_p_is_draw(){
        assertEquals("draw", RPS.chup('p', 'p'));
    }
    @Test
    public void testRPS_p1_h_p2_h_is_draw(){
        assertEquals("draw", RPS.chup('h', 'h'));
    }
    @Test
    public void testRPS_p1_r_p2_r_is_draw(){
        assertEquals("draw", RPS.chup('r', 'r'));
    }
        @Test
    public void testRPS_p1_s_p2_p_is_p1(){
        assertEquals("p1", RPS.chup('s', 'p'));
    }
    @Test
    public void testRPS_p1_p_p2_h_is_p1(){
        assertEquals("p1", RPS.chup('p', 'h'));
    }
    @Test
    public void testRPS_p1_h_p2_s_is_p1(){
        assertEquals("p1", RPS.chup('h', 's'));
    }
    @Test
    public void testRPS_p1_p_p2_s_is_p2(){
        assertEquals("p2", RPS.chup('p', 's'));
    }
    @Test
    public void testRPS_p1_h_p2_p_is_p2(){
        assertEquals("p2", RPS.chup('h', 'p'));
    }
    @Test
    public void testRPS_p1_s_p2_h_is_p2(){
        assertEquals("p2", RPS.chup('s', 'h'));
    }




}
